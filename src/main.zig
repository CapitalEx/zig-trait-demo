const std = @import("std");
const Allocator = std.mem.Allocator;

const WeaponType = enum {
    PerfectSword,
    GoodSword,
    BadSword
};

const Weapon = struct {
    weapon_type: WeaponType,

    fn power(self: *Weapon) i32 {
        return switch (self.weapon_type) {
            WeaponType.BadSword => 5,
            WeaponType.GoodSword => 10,
            WeaponType.PerfectSword => 25
        };
    }
};

const TraitTag = enum {
    HurtableTrait,
    HealableTrait,
};

const PropTag = enum {
    HealthProp,
    WeaponProp,
};

const Prop = union(PropTag) {
    HealthProp: i32,
    WeaponProp: Weapon,
};

const Trait = union(TraitTag) {
    HurtableTrait: *const fn (*i32, i32) void,
    HealableTrait: *const fn (*i32, i32) void,

};

const Entity = struct {
    traits: std.ArrayList(Trait),
    props: std.ArrayList(Prop),

    fn findTrait(self: *Entity, trait: TraitTag) ?*Trait {
        for (self.traits.items) |*t| {
            if (t.* == trait) {
                return t;
            }
        }

        return null;
    }

    fn findProp(self: *Entity, prop: PropTag) ?*Prop {
        for (self.props.items) |*p| {
            if (p.* == prop) {
                return p;
            }
        }

        return null;
    }
};

fn hurt(health: *i32, amount: i32) void {
    health.* -= amount;
}

fn heal(health: *i32, amount: i32) void {
    health.* += amount;
}


fn Player(alloc: Allocator, health: i32, weapon: Weapon) anyerror!Entity {
    var entity = Entity {
        .traits = std.ArrayList(Trait).init(alloc),
        .props  = std.ArrayList(Prop).init(alloc),
    };

    try entity.props.append(Prop{ .HealthProp = health });
    try entity.props.append(Prop{ .WeaponProp = weapon });

    try entity.traits.append(Trait{ .HurtableTrait = &hurt });
    try entity.traits.append(Trait{ .HealableTrait = &heal });
    return entity;
}

fn Enemy(alloc: Allocator, health: i32, weapon: Weapon) anyerror!Entity {
    var entity = Entity {
        .traits = std.ArrayList(Trait).init(alloc),
        .props = std.ArrayList(Prop).init(alloc),
    };

    try entity.props.append(Prop{ .HealthProp = health });
    try entity.props.append(Prop{ .WeaponProp = weapon });

    try entity.traits.append(Trait{ .HurtableTrait = &hurt });
    try entity.traits.append(Trait{ .HealableTrait = &heal });

    return entity;
}

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const alloc = arena.allocator();
    var player = try Player(alloc, 10, Weapon{ .weapon_type = WeaponType.GoodSword });

    var player_health_prop = player.findProp(PropTag.HealthProp);

    if (player.findTrait(TraitTag.HurtableTrait)) |player_hurt| {
        if (player_health_prop) |player_health| {
            player_hurt.HurtableTrait(&player_health.HealthProp, 3);
        }
    }

    std.log.info("{any}", .{player_health_prop});
}